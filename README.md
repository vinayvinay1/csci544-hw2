1. Accuracy of POS tagger on POS.DEV file is :  95.46%

2. 

LOC:

Precision :	68%
Recall :	72%
F-score :	70%

PER:

Precision :	85.8%
Recall :	68%
F-score :	76%

ORG:

Precision :	81.1%
Recall :	60.6%
F-score :	69.3%

MISC:

Precision :	74.7%
Recall :	35.2%
F-score :	48%



O:

Precision :	97.9%
Recall :	99%
F-score :	98.5%

Overall:

Precision :	95.8%
Recall :	95.8%
F-score :	95.8%




3.

I used NB for POS tagger instead of Averaged Perceptron. Below are the metrics :

Using NB :

Accuracy : 37935/40117 = 94.6%


Using Averaged Perceptron : 95.46% (based on point 1)