__author__ = 'vinaykumar'

import sys
import subprocess

fin = open(sys.argv[1], 'r', errors='ignore')
fnew = open("temporary.txt", 'w')

lines = fin.read().splitlines()
fin.close()


for line in lines:
    words = line.split()
    i = 0
    while i < len(words):
        tag = words[i].split("/")[1]
        current = words[i].split("/")[0]

        if i == 0:
            prev = "START"
            prev_tag = "START"
        else:
            prev = words[i-1].split("/")[0]
            prev_tag = words[i-1].split("/")[1]

        if i == len(words)-1:
            after = "END"
        else:
            after = words[i+1].split("/")[0]

        fnew.write(tag+" current:"+current+" prev:"+prev+" next:"+after+" prev_tag:"+prev_tag+" suffix:"+(current[-3:] or current[-2:] or current[-1:])+" prefix:"+(current[:2] or current[:1])+"\n")
        i += 1

fnew.close()
cmd = []
cmd.append("python")
cmd.append("perceplearn.py")
cmd.append("temporary.txt")
cmd.append(sys.argv[2])
subprocess.call(cmd)