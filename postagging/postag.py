__author__ = 'vinaykumar'

import sys
import pickle

fin = open(sys.argv[1], 'rb')
wavg, headers = pickle.load(fin)

lines = sys.stdin.read().splitlines()
classes = {}
output = []

for h in headers:
    classes.update({h: 0})


for line in lines:
    words = line.split()
    tagged_line = ""
    ptag = ""
    i = 0
    while i < len(words):
        op_classes = dict(classes)

        current = words[i]

        if i == 0:
            prev = "START"
            prev_tag = "START"
        else:
            prev = words[i-1]
            prev_tag = ptag

        if i == len(words)-1:
            after = "END"
        else:
            after = words[i+1]

        formatted_word = "current:"+current+" prev:"+prev+" next:"+after+" prev_tag:"+prev_tag+" suffix:"+(current[-3:] or current[-2:] or current[-1:])+" prefix:"+(current[:2] or current[:1])

        for h in headers:
            for word in formatted_word.split():
                if word in wavg[h]:
                    op_classes[h] += wavg[h][word]

        tagged_line += (current+"/")
        ptag = max(op_classes, key=op_classes.get)
        tagged_line += ptag
        if after != "END":
            tagged_line += " "

        i += 1

    print(tagged_line)
