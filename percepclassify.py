__author__ = 'vinaykumar'

import sys
import pickle

fin = open(sys.argv[1], 'rb')
wavg, headers = pickle.load(fin)
fin.close()

testdata = input()
classes = {}

for h in headers:
    classes.update({h: 0})

for h in headers:
    for word in testdata.split():
        if word in wavg[h]:
            classes[h] += wavg[h][word]

print(max(classes, key=classes.get))
sys.stdout.flush()

