__author__ = 'vinaykumar'

import sys
import pickle
import codecs

fin = open(sys.argv[1], 'rb')
wavg, headers = pickle.load(fin)


sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
lines = sys.stdin.read().splitlines()
classes = {}
output = []

for h in headers:
    classes.update({h: 0})


for line in lines:
    words = line.split()
    tagged_line = ""
    ptag = ""
    i = 0
    while i < len(words):
        op_classes = dict(classes)

        current = words[i].split("/")[0]
        current_pos = words[i].split("/")[1]

        if i == 0:
            prev = "START"
            prev_tag = "START"
            prev_pos = "START"
        else:
            prev = words[i-1].split("/")[0]
            prev_tag = ptag
            prev_pos = words[i-1].split("/")[1]

        if i == len(words)-1:
            after = "END"
            after_pos = "END"
        else:
            after = words[i+1].split("/")[0]
            after_pos = words[i+1].split("/")[1]

        formatted_word = "current:"+current+" prev:"+prev+" next:"+after+" prev_tag:"+prev_tag+" current_pos:"+current_pos+" prev_pos:"+prev_pos+" after_pos:"+after_pos

        for h in headers:
            for word in formatted_word.split():
                if word in wavg[h]:
                    op_classes[h] += wavg[h][word]

        tagged_line += (current+"/"+current_pos+"/")
        ptag = max(op_classes, key=op_classes.get)
        tagged_line += ptag
        if after != "END":
            tagged_line += " "

        i += 1

    print(tagged_line)
