__author__ = 'vinaykumar'

import sys
import subprocess

fin = open(sys.argv[1], 'r', errors='ignore')
fnew = open("temporary.txt", 'w')

lines = fin.read().splitlines()
fin.close()


for line in lines:
    words = line.split()
    i = 0
    while i < len(words):
        tag = words[i].split("/")[-1]
        current = words[i].rsplit("/", maxsplit=2)[-3]
        current_pos = words[i].split("/")[-2]

        if i == 0:
            prev = "START"
            prev_tag = "START"
            prev_pos = "START"
        else:
            prev = words[i-1].rsplit("/", maxsplit=2)[-3]
            prev_tag = words[i-1].split("/")[-1]
            prev_pos = words[i-1].split("/")[-2]

        if i == len(words)-1:
            after = "END"
            after_pos = "END"
        else:
            after = words[i+1].rsplit("/", maxsplit=2)[-3]
            after_pos = words[i+1].split("/")[-2]

        fnew.write(tag+" current:"+current+" prev:"+prev+" next:"+after+" prev_tag:"+prev_tag+" current_pos:"+current_pos+" prev_pos:"+prev_pos+" after_pos:"+after_pos+"\n")
        i += 1

fnew.close()
cmd = []
cmd.append("python")
cmd.append("perceplearn.py")
cmd.append("temporary.txt")
cmd.append(sys.argv[2])
subprocess.call(cmd)